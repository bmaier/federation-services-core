# Federation Services

*Core specification of functional and technical requirements*

## *Federation*

The conceptual model of Gaia-X enables and promotes the creation of the so-called Gaia-X Federations as a conceptual component.[^1]

Such Federations are self-determined ecosystems, where individual [Participants](https://gaia-x.gitlab.io/technical-committee/architecture-document/conceptual_model/#participants) join. A Federation refers to a loose set of interacting Particpants that directly or indirectly consume, produce, operate or provide related Services Instances. A Federation can be awarded as Gaia-X Federation by complying with the Gaia-X Rulebook[^2].

[^1]: https://gaia-x.gitlab.io/technical-committee/architecture-document//conceptual_model/
[^2]: Gaia-X Rulebook to be defined


## Federation Services

The Federation Services are the core functional services to operate a Federation, manged by one or more Federators. They enable and facilitate interoperability of Identities and additionally portability of Services within and across Gaia-X- Ecosystems . Futheron they ensure transparency between or among Participants, make Services searchable, discoverable and consumable, and provide means for Data Sovereignty in distributed environments.


## Interlinking Federation Services

Federation Services can be interlinked via the Gaia-X Trustframework.[^3] Beside the core requirement by Gaia-X, defined by the Gaia-X Trust Framework, a Federation is entitled to apply additional requirements.

[^3]: https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/

## Federation Governance vs. Federation Operations

To enable interoperability, portability and Data Sovereignty across different Federations and communities, Federation Services need to adhere to common standards. These standards must be unambiguous . The Gaia-X Association AISBL owns the Compliance Framework and related regulations or governance aspects. A comprehensive governance for Federations should address Business, Legal, Operational, Functional and Technical (BLOFT) aspects, while the federation services focus on the functional and technical aspects. Different entities may take on the role of Federator and Federation Services Provider.

## Defining Federations

A Federation is a group of Participants that work together and collaborate on equal and horizontal lines. The Federation is not owned by anyone. Instead, the Participants cooperate based on joint rules .

Participant of the Federation or external service supplier are appointed to become the so-called Federators, aiming to facilitate the group coordination and to provide for the necessary Federation Services implementation, required to operationalize the Federation. Federations can be across participants from the same industry or from different industries, thus implementing both vertical ecosystems ad well as horizontal (or meshed) ecosystems of data and infrastructures.

Participant Self-Descriptions are the basis for the Federations to provide transparency about involved organisations. Participants are requested to share information about their business, their profile data and if applicable (acting as a provider) high level information related to their service offering. 

Service Self-Descriptions are intended to describe the Characteristics of consumable services. Each Federation can create and manage its own Service Catalogue – a collection of the services offered within this Federation. The access rights are set by the Federation based on core governance rules and can be linked with other Service Catalogues for generic public service offerings private offerings by interconnected federations.

## The role of Federation Services 

The Federation Services is to be seen as the implementation of a toolbox – providing for the minimum technical requirements to empower a Federation to become operational. Concretely, the first set of services that will be delivered are:
- **Identity and Trust**: Services based on the Self-Sovereign Identity (SSI) concept enables handling of decentralised identities and digital trust establishments for identities and assets. The decentralised identity management based on W3C Verifiable Credentials and Distributed Identifier (DID) enables Participants to keep control over their digital identities.

- **Federated Catalogue**: The Federated Catalogue will be the repository of one Federation to manage Self-Descriptions and enabling participants to find other Participants’ information and service offerings in the shape of the Self-Descriptions. 
Self-Descriptions express characteristics of Resources, Service Offerings and Participants that
are linked to their respective Identifiers. Providers are responsible for the creation of SelfDescriptions of their Resources. In addition to self-declared Claims made by Participants
about themselves or about the Service Offerings provided by them, a Self-Description may
comprise verifiable credentials issued and signed by trusted parties.

- **Data Sovereignty Services**:Data Sovereignty Services enables to Federation to coordinate the relationships of data service provider and data service consumer and to keep track on exchange events. They give participants the capability to support self-determination of their data exchange and sharing mechanism. Informational self-determination for Participants includes two aspects within the data sharing concept:
    - (1) Transparency
    - (2) Control of data usage.

- **Compliance**: The main objective of the Compliance Federation Service is to provide Gaia-X users with verification of Compliance to the stated characteristics for each of the specific Service Offerings.

## Architecture Requirements

The following table presents how the Federation Services contribute to the Architecture Requirements that are mentioned in section [Architecture Requirements](https://gaia-x.gitlab.io/technical-committee/architecture-document/overview/#architecture-requirements).

| Requirement | Relation to the Federation Services  |
|-------------|--------------------------------------|
| Interoperability  | <ul><li>The Federated Catalogues ensure that Providers offer services through the whole technology stack. The common Self-Description scheme also enables interoperability.</li><li> A shared Compliance Framework and the use of existing standards supports the combination and interaction between different Resources.</li><li>The Identity and Trust mechanisms enable unique identification in a federated, distributed setting</li><li>The possibility to exchange data with full control and enforcement of policies as well as logging options encourages Participants to do so</li></ul> |
| Portability | <ul><li>The Federated Catalogues encourage Providers to offer Resources with transparent Self-Descriptions and make it possible to find the right kind of service that is “fit for purpose” and makes the interaction possible.</li><li>Common compliance levels and the re-use of existing standardssupports portability of data and services.</li></ul>  | 
| Sovereignty | <ul><li>Identity and Trust provide the foundation for privacy considerations as well as access and usage rights. Standards for sovereign dataexchange enable Usage Policies. The Self-Descriptions offer the opportunity to specify and attach Usage Policies for Data Resources.</li></ul>  |
| Security and Trust | <ul><li>The Architecture and Federation Services provide definitions for trust mechanisms that can be enabled by different entities and enable transparency.</li><li>Sovereign Data Exchange, as well as Compliance concerns address security considerations. The identity and trust mechanisms provide the basis. The Federated Catalogues present Self-Descriptions and provide transparency over Service Offerings.</li></ul>  |

## Standards for Federation Services

| Category  | Short name | Governing Body | Source Link | Comments |
|-----------|------------|----------------|-------------|----------|
| Identity  | DID-Core Model  | W3C  | https://www.w3.org/TR/did-core/  |   |
| Trust  | Verifiable Credentials  | W3C   | https://www.w3.org/TR/vc-data-model/  |   |
| Identity  | OIDC  | Open ID Foundation  | https://openid.net/connect/  | Associated specs to consider <p>https://openid.net/specs/openid-connect-4-verifiable-presentations-1_0.html</p> <p>https://openid.net/specs/openid-connect-4-verifiable-presentations-1_0.html</p>  |
| Catalogue and Self Description  | JSON-LD  | W3C  | https://www.w3.org/TR/2014/REC-json-ld-api-20140116/  |   |
| Sovereign Data Exchange  | LDP – Linked Data Platform  | W3C  | https://www.w3.org/TR/ldp/  |   |
| Sovereign Data Exchange   | LDN – Linked Data Notification  | W3C   | https://www.w3.org/TR/ldn/  |   |
| Sovereign Data Exchange   | ODRL – Open Digital Rights Language Model  | W3C  | https://www.w3.org/TR/odrl-model/  |   |
| Sovereign Data Exchange   | ODRL – Open Digital Rights Language Vocabulary  | W3C | https://www.w3.org/TR/odrl-vocab/  |   |

## Interoperability and Portability concepts

Core to the Gaia-X mission, we reference the international understanding of what these terms mean and what they require to be solved.

| Standard | Function | Document source | 
|----------|----------|-----------------|
| ISO/IEC 19941:2017 Information technology — | Describes what these terms refer to, and dives down into the various facets that must be  | [Link](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)  | 


| Initiative| Function | Document source | 
|-----------|----------|-----------------|
| <p>Switching & Porting</p>  | SWIPO (Switching Cloud Providers and Porting Data), is a multi-stakeholder group facilitated by the European Commission, in order to develop voluntary Codes of Conduct for the proper application of the EU Free Flow of Non-Personal Data Regulation / Article 6 "Porting of Data"  |   https://swipo.eu/code-adherence/ |

ISO/IEC 19941:2017 includes definitions and discussion of the following facets:
- Interoperability (two systems communicating with one another)
    - Transport interoperability
    - Syntactic interoperability
    - Semantic interoperability
    - Behavioral interoperability
    - Policy interoperability
- Data portability (migrating data from one system to another)
    - Syntactic data portability
    - Semantic data portability
    - Policy data portability
- Application portability (migrating executable code from one system to another)
    - Syntactic application portability
    - Semantic application portability
    - Policy application portability

The standard also discusses issues such as security risks and challenges that can arise during migration of data and applications.

The European Interoperability framework EIF[^4] (http://ec.europa.eu/isa2/sites/isa/files/eif_brochure_final.pdf ) proposes four layers for interoperability, Legal Interoperability, Organizational Interoperability, Semantic Interoperability and technical interoperability. Gaia-X and the Federation Services address the four layers partly, while a federation should add significantly to the four layers. 

The scope of the Federation services is technical interoperability for data and services (both to limited extend, to be detailed) and semantic interoperability as far as addressed in the self-descriptions. 

[^4]: https://ec.europa.eu/isa2/sites/default/files/eif_brochure_final.pdf
